# Pixler

A simple nodejs icon api which gives you the perfect icons which you want for your projects.

## PART I: Download & Build on local

## Method: From github

### 1) Clone the repository, install node packages and verify routes locally

```
//on local
git clone https://github.com/SaurabhSonde/Pixler.git
cd Pixler
npm install
npm start
```

Open your local browser and verify the pixler is working by accessing:  
`http://localhost:5000/icon`
