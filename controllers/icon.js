const Icon = require("../models/icon");
const multer = require("multer");
const { GridFsStorage } = require("multer-gridfs-storage");
const mongoose = require("mongoose");

// multer gridfsstorage
const storage = new GridFsStorage({
  url: process.env.Mongo_Uri,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    const { iconName } = req.body;
    const match = ["image/png", "image/jpeg", "image/svg+xml"];

    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${iconName}`;
      return filename;
    }

    return {
      bucketName: "icons",
      filename: `${file.originalname}`,
    };
  },
});

// multer middleware
exports.uploadImg = multer({ storage: storage }).array("image");

// POST Icons
exports.addIcon = (req, res) => {
  const { tags } = req.body;
  req.files.map(async (icon, index) => {
    try {
      let id = mongoose.Types.ObjectId();
      const schema = {
        _id: id,
        iconName: icon.originalname,
        URL: `http://localhost:5000/api/v1/icon/${id}`,
        mimeType: icon.mimetype,
        description: "",
        category: "",
        subCategory: "",
        tags: tags,
      };
      const data = await Icon.create(schema);
      res.status(200).json(data);
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        error: "Server Error",
      });
    }
  });
};

// get all icons
exports.getIcons = (req, res) => {
  let { page, per_page } = req.query;

  // If the page is not applied in query
  if (!page) {
    // Make the Default value one
    page = 1;
  }
  // If the per_page is not applied in query
  if (!per_page) {
    per_page = 5;
  }

  const limit = parseInt(per_page);

  Icon.find()
    .sort({ votes: 1, _id: -1 })
    .limit(limit)
    .exec((err, icons) => {
      if (err) {
        return res.status(404).json({
          error: "Icons not found",
        });
      }

      res.json({ page, per_page, icons });
    });
};

// get icon by id
exports.getIconsById = async (req, res) => {
  const id = req.params.id;
  const data = await Icon.findById(id);
  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
    bucketName: "icons",
  });
  const head = {
    "Content-Type": "image/svg+xml",
  };
  res.writeHead(206, head);
  bucket.openDownloadStreamByName(data.iconName).pipe(res);
};

// get icons by search
exports.getIconsBySearch = async (req, res) => {
  try {
    let { query, page, per_page } = req.query;

    if (!page) {
      page = 1;
    }

    if (!per_page) {
      per_page = 5;
    }

    const limit = per_page;
    const data = await Icon.find({ tags: query })
      .collation({
        locale: "en",
        strength: 2,
      })
      .sort({ votes: 1, _id: -1 })
      .limit(limit);
    return res.status(200).json(data);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: "Server Error",
    });
  }
};
