const mongoose = require("mongoose");

const iconSchema = new mongoose.Schema(
  {
    iconName: {
      type: String,
      required: true,
    },
    URL: {
      type: String,
      required: true,
    },
    mimeType: {
      type: String,
    },
    description: {
      type: String,
    },
    category: {
      type: String,
    },
    subCategory: {
      type: String,
    },
    tags: [String],
  },
  { timestamps: true }
);

module.exports = mongoose.model("Icon", iconSchema);
